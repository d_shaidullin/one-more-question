﻿#include <iostream>
#include <string>

using namespace std;

class A
{
public:

    A()
    {
        color = "red";
        foot_num = 0;
    }

    A(string _color, int _foot_num)
    {
        color = _color;
        foot_num = _foot_num;
    }

    ~A()
    {

    }

    void info()
    {
        cout << color << ' ' << foot_num << "\n";
    }
private:

    string color = " ";
    int foot_num = 0;

};






int main()
{
    A cars[] = { A("red", 4), A("black", 2), A("blue", 3) };
    for (int i = 0; i < 3; i++)
    {
        cars[i].info();
    }

    cout << endl;

    cars[0].~A();

    for (int i = 0; i < 3; i++)
    {
        cars[i].info();
    }
}

